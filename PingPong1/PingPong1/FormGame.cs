﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PingPong1
{
    public partial class FormGame : Form
    {
        public int px1 = 0, px2 = 0;
        //
        public FormGame(int px1, int px2)
        {
            this.px1 = px1;
            this.px2 = px2;
            InitializeComponent();
            loadSize = Size;
            Text = px1 + " - " + px2;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Location = new Point(200, 0);
            timerGo.Start();
            timerChak.Start();
        }
        int speadBol = 5;
        int[] speadUsers = { 8, 8 };
        bool stop = false, side = true;
        bool[] upDown = new bool[2], startMove = {false, false};
        bool stopBol = false;
        private void timerGo_Tick(object sender, EventArgs e)
        {
            if (stop)
                return;
            if (!stopBol)
            {
                if (Bol.Location.Y <= 0 || Bol.Location.Y + Bol.Size.Height + 25 >= loadSize.Height)
                {
                    goY *= -1;
                }
                if (side)
                {
                    if (Bol.Location.X + 40 + speadBol < Size.Width)
                        move();
                }
                else
                {
                    if (Bol.Location.X - speadBol > 0)
                        move();
                }
            }
            Panel[] users = { User1, User2 };
            for (int userI = 0; userI < 2; userI++)
            {
                var itme = users[userI];
                if (startMove[userI])
                {
                    if (upDown[userI])
                    {
                        if (itme.Location.Y > 0)
                            itme.Location = new Point(itme.Location.X, itme.Location.Y - speadUsers[userI]);
                    }
                    else
                    {
                        if (itme.Location.Y + itme.Size.Height + 25 + speadUsers[userI] < Size.Height)
                            itme.Location = new Point(itme.Location.X, itme.Location.Y + speadUsers[userI]);
                    }
                }
            }
        }
        private void timerChak_Tick(object sender, EventArgs e)
        {
            if (stop)
                return;
            const int lak = 1;
            BoxPrint.Text = Bol.Location.X + "";
            if (Bol.Location.X == User1.Location.X + User1.Size.Width)
            {
                if (Bol.Location.Y + Bol.Size.Height - lak > User1.Location.Y + Bol.Size.Height - lak && Bol.Location.Y + lak < User1.Location.Y + User1.Size.Height)
                {
                    Bol.BackColor = User1.BackColor;
                    Collision(Bol.Location.Y - User1.Location.Y + Bol.Size.Height / 2 - uIntLocal);
                }
                else
                {
                    End(2);
                }
            }
            if (Bol.Location.X + Bol.Size.Width == User2.Location.X)
            {
                if (Bol.Location.Y + Bol.Size.Height - lak > User2.Location.Y && Bol.Location.Y + lak < User2.Location.Y + User2.Size.Height)
                {
                    Bol.BackColor = User2.BackColor;
                    Collision(Bol.Location.Y - User2.Location.Y + Bol.Size.Height / 2 - uIntLocal);
                }
                else
                {
                    End(1);
                }
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.P)
                stop = !stop;
            
            if (stop)
                return;
            
            switch (e.KeyCode)
            {
                case Keys.A:
                    {
                        upDown[0] = true;
                        startMove[0] = true;
                    }
                    break;
                case Keys.Z:
                    {
                        upDown[0] = false;
                        startMove[0] = true;
                    }
                    break;
                case Keys.X:
                    {
                        upDown[0] = !upDown[0];
                        startMove[0] = true;
                    }
                    break;
                case Keys.Up:
                    {
                        upDown[1] = true;
                        startMove[1] = true;
                    }
                    break;
                case Keys.Down:
                    {
                        upDown[1] = false;
                        startMove[1] = true;
                    }
                    break;
                case Keys.Left:
                    {
                        upDown[1] = !upDown[1];
                        startMove[1] = true;
                    }
                    break;
                case Keys.Escape:
                    {
                        Program.stop = false;
                        Close();
                    }
                    break;
            }
        }
        Size loadSize;
        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            Size = loadSize;
        }
        
        const int uIntLocal = 72;

        int goX = 1, goY = 0;
        public void Collision(int local = 0)
        {
            goY = local / 7;
            side = !side;
        }
        public void move()
        {
            int sideNumber = (side) ? 1 : -1;
            Bol.Location = new Point(Bol.Location.X + speadBol * goX * sideNumber, Bol.Location.Y + goY);
        }
        public void End(int win)
        {
            if (win == 1)
            {
                MessageBox.Show("blue win");
                px1++;
            }
            else
            {

                MessageBox.Show("red win");
                px2++;
            }
            Close();
        }
    }
}
